//soal 1
const persegi_panjang = (p,l) =>{
    let keliling;
    let luas;
    keliling = (2*(p+l));
    luas = p * l;
    return {luas,keliling};
}
let con1 = 5;
let con2 = 7;
let x;
x = persegi_panjang(con1,con2);
console.log(x);

//soal 2

const newFucntion = (firstname,lastname) => {
    return{firstname,lastname,
        fullname : function(){
            console.log(firstname+" "+lastname)
        }
    }
}
newFucntion("William","Imoh").fullname()

//soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  const {firstName,lastName,address,hobby} = newObject
console.log(firstName, lastName, address, hobby)

//soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

//soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
console.log(before)
const after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(after)